data "ibm_container_cluster_config" "cluster_config" {
  cluster_name_id   = var.cluster_id
  
	resource_group_id = data.ibm_resource_group.resource_group.id 

  # Required for getting the calico configuration
  admin           = "true"
  network         = "true"
  config_dir      = "/tmp"

}
resource "null_resource" "delete-deployment" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/delete_elements_bytag.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "topaz-deployment-app"
        EOT
        on_failure = continue
  }
    depends_on = [ data.ibm_container_cluster_config.cluster_config ]
}

resource "null_resource" "apply-oracle-yaml" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "oracle12c.yaml"
        EOT
    }    
    depends_on = [ null_resource.delete-deployment]
}

resource "time_sleep" "wait_for_oracledb" { 

    depends_on = [ null_resource.apply-oracle-yaml]
    create_duration = "300s"
}


resource "null_resource" "apply-topaz-yaml" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "deploy-job.yaml"
        EOT
    }
    depends_on = [ time_sleep.wait_for_oracledb ]
}

resource "null_resource" "apply-hpa-yaml" {
    provisioner "local-exec" {
        command = <<EOT
            ./scripts/apply_yaml.sh "${data.ibm_container_cluster_config.cluster_config.config_file_path}" "hpa.yaml"
        EOT
    }
    depends_on = [ null_resource.apply-topaz-yaml ]
}